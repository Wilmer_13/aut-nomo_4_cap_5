#__-----------------------------------UNIVERSIDAD NACIONAL DE LOJA-------------------------------------------
#__Escribe un programa que pida una lista de números y al final muestre por pantalla el máximo y mínimo de losS
#números.

#__Author: Wilmer Cruz
#__Email: wilmer.cruz@unl.edu.ec


lista =[]
valores = int(input("Ingrese la cantidad valores a comparar: "))
secuencia = 1
mayor = 0
menor = 0

while (valores > 0):
    numero = float (input ("Número: " + str (secuencia) + ": "))
    lista.append(numero)
    secuencia = secuencia + 1
    valores = valores - 1

mayor = max(lista)
menor = min(lista)

print ("Los números ingeresados de la lista son: ", lista)
print ("El número mayor de la lista es: ", mayor)
print ("El número menor de la lista es: ", menor)


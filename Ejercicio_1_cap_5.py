#__--------------UNIVERSIDAD NACIOMAL DE LOJA------------------------
#Escribe un programa que lea repetidamente números hasta
#que el usuario introduzca “fin”. Una vez se haya introducido “fin”,
#muestra por pantalla el total, la cantidad de números y la media de
#esos números. Si el usuario introduce cualquier otra cosa que no sea un
#número, detecta su fallo usando try y except, muestra un mensaje de
#error y pasa al número siguiente.

#__Author: Wilmer Cruz
#__Email: wilmer.cruz@unl.edu.ec


lista = []
while True:
    numero = input("Introduzca un número: ")
    if numero.lower() in "fin":
        break
    try:
        lista.append(int(numero))
    except ValueError:
        print("Entrada inválida")

print("La suma de los números es: {}".format(sum(lista)))
print("El total de números introducidos es: {}".format(len(lista)))
print("El promedio de los números introducidos es: {}".format(sum(lista) / len(lista)))


